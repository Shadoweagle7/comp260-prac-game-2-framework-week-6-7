﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

    private Rigidbody PaddleRigidBody;

    public float Speed = 20.0f, Force = 10f;

    private Vector3 GetMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);

        return ray.GetPoint(distance);
    }

	// Use this for initialization
	void Start () {
        this.PaddleRigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Time: " + Time.time);
	}

    private void FixedUpdate()
    {
        Debug.Log("Fixed Time: " + Time.fixedTime);

        Vector3 pos = GetMousePosition(), dir = pos - this.PaddleRigidBody.position, vel = dir.normalized * this.Speed;

        float move = this.Speed * Time.fixedDeltaTime, distanceToTarget = dir.magnitude;

        if (move > distanceToTarget)
        {
            vel = vel * distanceToTarget / move;
        }

        this.PaddleRigidBody.AddForce(dir.normalized * this.Force);
        this.PaddleRigidBody.velocity = vel;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(Camera.main.transform.position, GetMousePosition());
    }
}
